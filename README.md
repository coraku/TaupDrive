# === Projet TaupDrive ===

    Pour ce projet, nous avons décidé de faire une application Drive dans laquelle il y aura des articles.
    Il sera possible sur ces articles d'en choisir pour les ajouter à un panier.
    


## Besoin pour le développement 




## Description des fonctionnalités attendues

- Consultation des articles disponibles.


## Gestion des tâches 

    Chaque personne de notre groupe s'est vu affecter différentes tâches pour réaliser ce projet.
    
    
### IMBERT Guillaume


### MOUTINHO Jérémie


### RAVIT Victor

- Gestion du design général de l'application.


### ANNUNZIATA Nicolas

- Gestion de la base de données des articles.
- Gestion de la documentation du projet.



